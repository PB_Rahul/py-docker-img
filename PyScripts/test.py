import csv
import requests
import shutil
from zipfile import ZipFile
import sys


print ("executed")
#foldername = 'BigQueryAPI'
filepath = 'CSVAPIList.csv'
with open (filepath,'rt') as f:
    listedAPI = csv.reader(f)
    for row in listedAPI:
        print (row[0],row[1])
        #downloadzipped(row[0],row[1])
        
url = 'https://api.enterprise.apigee.com/v1/organizations/rahulshrivastava05/apis/Banking-API/revisions/4?format=bundle'

s = requests.session()



headers = { 'Authorization' : 'Basic cmFodWxzaHJpdmFzdGF2YTA1QG91dGxvb2suY29tOlJAaHVsMTIz' }

resp = requests.get(url, headers = headers)


if resp.status_code != 200:
    print ("Status: ", resp.status_code)
    print ("User Not Authorized")
    sys.exit(1)
else:
    print("Success\n")
    print(resp.status_code)
response = requests.get(url=url, headers = headers, stream=True)
foldername = 'Banking-API.zip'
print ("Starting download Banking-API")
with open(foldername, 'wb') as out_file:
    shutil.copyfileobj(response.raw, out_file)
    print(out_file)

print (foldername)

# Create a ZipFile Object and load sample.zip in it
with ZipFile(foldername, 'r') as zipObj:
# Extract all the contents of zip file in current directory
    zipObj.extractall('APIs/')
    print ("Executed")
response.close
resp.close
s.close

print ("Im done")