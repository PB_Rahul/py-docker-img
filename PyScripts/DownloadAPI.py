import csv
import shutil
from zipfile import ZipFile
import sys
import requests
import subprocess
import git
import os


print ("executed")
#foldername = 'BigQueryAPI'
filepath = 'CSVList/CSVAPIList.csv'
with open (filepath,'rt') as f:
    listedAPI = csv.reader(f)
    for row in listedAPI:
        print (row[0],row[1])
        #downloadzipped(row[0],row[1])
        
url = 'https://api.enterprise.apigee.com/v1/organizations/rahulshrivastava05/apis/Banking-API/revisions/4?format=bundle'

s = requests.session()
headers = { 'Authorization' : 'Basic cmFodWxzaHJpdmFzdGF2YTA1QG91dGxvb2suY29tOlJAaHVsMTIz' }
resp = requests.get(url, headers = headers)

if resp.status_code != 200:
    print ("Status: ", resp.status_code)
    print ("User Not Authorized")
    sys.exit(1)
else:
    print("Success\n")
    print(resp.status_code)
response = requests.get(url=url, headers = headers, stream=True)
foldername = 'Banking-API'+'.zip'
print ("Starting download Banking-API")
with open(foldername, 'wb') as out_file:
    shutil.copyfileobj(response.raw, out_file)
    print(out_file)

print (foldername)
p = './PyScripts/%s'%(foldername)
# Create a ZipFile Object and load sample.zip in it
with ZipFile(foldername, 'r') as zipObj:
# Extract all the contents of zip file in current directory
    zipObj.extractall(p)
    str(zipObj)
#Below give Permission denied error
    #zipObj.extractall('/APIs')
    print ("Extraction completed")

#Below subprocess.run 
    out = subprocess.run(['sh', 'Scripts/dld.sh','%s',str(zipObj.extractall())],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT)
    #stdout,stderr = out.communicate()
    
    print (out.stdout)
    print ("Executed")
response.close
resp.close
s.close

print ("Im done")